package com.zhongan.doc.swagger.action.user;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zhongan.doc.swagger.model.ResultInfo;
import com.zhongan.doc.swagger.model.UserInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 类UserAction.java的实现描述：微信公众平台用户信息接口
 * 
 * @author: tongyufu 2017-12-04 15:25:17
 */
@Api(tags = "用户信息", description = "微信公众平台用户信息接口")
@RestController
@RequestMapping("/user")
public class UserAction {

	@ApiOperation(value = "查询指定ID用户")
	@GetMapping("/{id}")
	public ResultInfo<UserInfo> findById(@ApiParam(value = "用户ID", required = true) @PathVariable("id") Integer id) {
		return new ResultInfo<UserInfo>(new UserInfo(id, "用户" + id));
	}

	/**
	 * 此方法没有在@RequestMapping中指定httpMethod，所以需要在@ApiOperation中指定，否则会生成 "GET", "HEAD",
	 * "POST", "PUT", "DELETE", "OPTIONS" 和 "PATCH" 7个接口。
	 */
	@ApiOperation(value = "创建用户", notes = "根据User对象创建用户", httpMethod = "POST")
	@RequestMapping("insert")
	public ResultInfo<String> insert(@RequestBody UserInfo user) {
		return new ResultInfo<String>("添加新用户成功");
	}

	@ApiOperation(value = "修改用户")
	@ApiResponses({ @ApiResponse(code = 1001, message = "用户不存在"), @ApiResponse(code = 1002, message = "该用户不允许修改") })
	@PutMapping("update")
	public ResultInfo<String> update(@RequestBody UserInfo user) {
		if (user.getId() == 0) {
			return new ResultInfo<>("1001", "用户不存在");
		}
		if (user.getId() == -1) {
			return new ResultInfo<>("1002", "该用户不允许修改");
		}
		return new ResultInfo<String>("修改新用户成功");
	}

	@ApiOperation(value = "搜索用户")
	@GetMapping("search")
	public ResultInfo<List<UserInfo>> search(@ApiParam("用户ID") @RequestParam("id") Integer id,
			@ApiParam("用户姓名") @RequestParam("username") String username) {
		List<UserInfo> users = Arrays.asList(new UserInfo(1, "用户1"), new UserInfo(2, "用户2"));
		return new ResultInfo<List<UserInfo>>(users);
	}

}
