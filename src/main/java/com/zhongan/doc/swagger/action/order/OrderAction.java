package com.zhongan.doc.swagger.action.order;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zhongan.doc.swagger.model.OrderInfo;
import com.zhongan.doc.swagger.model.ResultInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 类UserAction.java的实现描述：微信公众平台订单信息接口
 * 
 * @author: tongyufu 2017-12-04 15:25:17
 */
@RestController
@Api(tags = "订单信息", description = "微信公众平台订单信息接口")
@RequestMapping("/order")
public class OrderAction {

	@ApiOperation(value = "查询指定ID订单")
	@GetMapping("/{id}")
	public ResultInfo<String> findById(@ApiParam("订单ID") @PathVariable("id") Integer id) {
		return new ResultInfo<String>("订单:" + id);
	}

	@PostMapping("insert")
	// @ApiOperation(value = "创建订单", notes = "根据Order对象创建订单")
	public ResultInfo<String> insert(@RequestBody OrderInfo order) {
		return new ResultInfo<String>("添加新订单成功");
	}

}
