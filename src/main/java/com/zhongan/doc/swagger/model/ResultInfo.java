package com.zhongan.doc.swagger.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 执行结果
 * 
 * @author tongyufu
 *
 */
@ApiModel("响应结果")
public class ResultInfo<T> extends BaseInfo {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty("是否成功")
	private boolean success = false;
	@ApiModelProperty("错误代码")
	private String errorCode;
	@ApiModelProperty("错误信息")
	private String errorMsg;
	@ApiModelProperty("响应业务数据")
	private T data;

	public ResultInfo() {
	}

	/** 成功信息 */
	public ResultInfo(T data) {
		success = true;
		this.data = data;
	}

	/** 失败信息 */
	public ResultInfo(String errorCode, String errorMsg) {
		success = false;
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
