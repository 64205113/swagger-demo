package com.zhongan.doc.swagger.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 类OrderInfo.java的实现描述：
 * 
 * @author: tongyufu 2017-12-05 09:52:15
 */
@ApiModel("订单信息")
public class OrderInfo extends BaseInfo {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "订单编号", required = true)
	private String orderNo;
	@ApiModelProperty(value = "订单详情", required = true)
	private String detail;

	public OrderInfo() {
	}

	public OrderInfo(String orderNo, String detail) {
		this.orderNo = orderNo;
		this.detail = detail;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

}
