package com.zhongan.doc.swagger.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 类UserModel.java的实现描述：
 * 
 * @author: tongyufu 2017-12-04 15:40:29
 */
@ApiModel("用户信息")
public class UserInfo extends BaseInfo {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty("用户编号")
	private Integer id;
	@ApiModelProperty(value = "用户姓名", required = true)
	private String username;

	public UserInfo() {
	}

	public UserInfo(Integer id, String username) {
		this.id = id;
		this.username = username;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
